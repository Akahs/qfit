function varargout = QFit(varargin)
% QFIT MATLAB code for QFit.fig
%      QFIT, by itself, creates a new QFIT or raises the existing
%      singleton*.
%
%      H = QFIT returns the handle to a new QFIT or the handle to
%      the existing singleton*.
%
%      QFIT('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in QFIT.M with the given input arguments.
%
%      QFIT('Property','Value',...) creates a new QFIT or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before QFit_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to QFit_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help QFit

% Last Modified by GUIDE v2.5 27-Oct-2016 18:12:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @QFit_OpeningFcn, ...
                   'gui_OutputFcn',  @QFit_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before QFit is made visible.
function QFit_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to QFit (see VARARGIN)

% Choose default command line output for QFit
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes QFit wait for user response (see UIRESUME)
% uiwait(handles.figure1);
global xmin xmax startPt folder
folder = './';
xmin = 800;
xmax = 1200;
startPt = 1000;
set(handles.textQt,'String','0');
set(handles.textQr,'String','0');
set(handles.textQa,'String','0');



% --- Outputs from this function are returned to the command line.
function varargout = QFit_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function loadB_Callback(hObject, eventdata, handles)
% hObject    handle to loadB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global f bg folder
path = strcat(folder,'\*.*');
[file,dir]=uigetfile(path);
path=strcat(dir,file);
M=dlmread(path);
f=M(:,1);
bg=M(:,2);
set(handles.textBg,'String',file);



% --------------------------------------------------------------------
function loadS_Callback(hObject, eventdata, handles)
% hObject    handle to loadS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global f sp folder
path = strcat(folder,'\*.*');
[file,dir]=uigetfile(path);
path=strcat(dir,file);
M=dlmread(path);
f=M(:,1);
sp=M(:,2);
set(handles.textSp,'String',file);


% --- Executes on button press in pushFit.
function pushFit_Callback(hObject, eventdata, handles)
% hObject    handle to pushFit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global spectrum bg sp
if isempty(sp)
    msgbox('Raw data not found! Please load data.','Warning');
elseif isempty(bg)
    spectrum = sp;
    QFitF(handles);
else
    spectrum = sp./bg;
    QFitF(handles);
end


% --------------------------------------------------------------------
function Untitled_2_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function xlim_Callback(hObject, eventdata, handles)
% hObject    handle to xlim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global xmin xmax
temp = inputdlg({'xmin','xmax'},'x limit',1,{num2str(xmin) num2str(xmax)});
if ~isempty(temp)
    xmin=str2double(temp(1));
    xmax=str2double(temp(2));
    QFitF(handles);
end


% --------------------------------------------------------------------
function selFolder_Callback(hObject, eventdata, handles)
% hObject    handle to selFolder (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global folder
folder = uigetdir(folder);
% display folder



% --------------------------------------------------------------------
function unload_Callback(hObject, eventdata, handles)
% hObject    handle to unload (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global sp folder
clearvars -global sp folder
set(handles.textSp,'String','None');
set(handles.textBg,'String','None');



% --------------------------------------------------------------------
function startP_Callback(hObject, eventdata, handles)
% hObject    handle to startP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global startPt
answer = inputdlg('Please enter a start point:','Input',1,{num2str(startPt)});
startPt = str2double(answer{:});
QFitF(handles);
