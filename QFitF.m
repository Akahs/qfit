function QFitF(handles)
    global f spectrum xmin xmax startPt
    if numel(f) ~= numel(spectrum)
        msgbox('Variable dimensions not match!')
    else
        target = @(k,b,a,sigma,x0,x) -a*exp(-(x-x0).^2/(2*sigma.^2))+k*x+b;
        [result,gof]= fit(f,spectrum,target,'StartPoint',[0,0.4,1,30,startPt],'Exclude',f<xmin|f>xmax);
        plot(result,f,spectrum);
        xlim([xmin xmax])
        coefs=coeffvalues(result);
        k=coefs(1);
        b=coefs(2);
        a=coefs(3);
        sigma=coefs(4);
        x0=coefs(5);
        % [tmp,ub]=min(abs(f-xmin));
        % [tmp,lb]=min(abs(f-xmax));
        T1=k*x0+b;
        T2 = T1-a;
        % [T2,iRes]=min(spectrum(lb:ub));
        T=T2/T1;
        fwhm = 2.3548*sigma;
        Q = x0/fwhm;
        Q_rad = Q/(1-sqrt(T));
        Q_abs = Q/sqrt(T);
        set(handles.textQt,'String',num2str(Q,4));
        set(handles.textQr,'String',num2str(Q_rad,4));
        set(handles.textQa,'String',num2str(Q_abs,4));
        % set(handles.textFr,'String',num2str(f(iRes+lb),5));
        set(handles.textFr,'String',num2str(x0,4));
        % set(handles.textT1,'String',num2str(T1,4));
        % set(handles.textT2,'String',num2str(T2,4));
        set(handles.textR,'String',num2str(gof.rsquare,4));
    end